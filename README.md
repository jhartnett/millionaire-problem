# Millionaire Problem

This project is an implementation of the [Fairplay Protocol](https://www.cs.huji.ac.il/~noam/fairplay.pdf) 
which is a solution to [Yao's Millionaire Problem](https://en.wikipedia.org/wiki/Yao%27s_Millionaires%27_Problem).
The goal is to allow two parties to compare their wealth without revealing their
inputs to one another or trusting a third party. This solution actually solves
the more general problem of two party secure binary circuit computation.

The SecureComputation project contains the general solution, while the Millionaire project
contains the application to Yao's Millionaire problem. The solution as provided
uses one program for both inputs and outputs for simplicity. In a real scenario,
each party would use separate programs on their own computers and connect to one
another.

Of special note is the circuit building API, which is displayed in `SecureComputation/Circuits.cs`.
Operator overloading is utilized to abstract the details of circuit building away.

This NOT FOR USE in a production scenario. In any case where real security is 
needed, you should only rely on heavily scrutinized open-source projects. 
Although this implementation has been tested, it does not meet the scrutiny 
criteria for real world use.