﻿using SecureComputation.Utils;

namespace SecureComputation.Data
{
    public class EncryptedEntry : ISerializable
    {
        private readonly byte[] data;

        public EncryptedEntry(Config config)
        {
            data = new byte[config.HashSize];
        }

        public EncryptedEntry(Config config, byte[] data)
            : this(config)
        {
            this.data.Copy(data);
        }

        public EncryptedEntry(Config config, Connection conn)
            : this(config)
        {
            Read(conn);
        }

        public int Size => data.Length;
        public byte this[int index]
        {
            get
            {
                if(index < 0)
                    index += Size;
                return data[index];
            }
            set => data[index] = value;
        }

        public void Read(Connection conn) => conn.ReadBytes(this);
        public void Write(Connection conn) => conn.WriteBytes(this);

        public static implicit operator byte[](EncryptedEntry entry) => entry?.data;
    }
}
