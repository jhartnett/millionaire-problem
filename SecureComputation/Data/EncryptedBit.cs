﻿using SecureComputation.Utils;

namespace SecureComputation.Data
{
    public sealed class EncryptedBit : ISerializable
    {
        private readonly byte[] data;

        public EncryptedBit(Config config)
        {
            data = new byte[config.VarSize + 1];
        }

        public EncryptedBit(Config config, byte[] data)
            : this(config)
        {
            this.data.Copy(data);
        }

        public EncryptedBit(Config config, Connection conn)
            : this(config)
        {
            Read(conn);
        }

        public int Size => data.Length;

        public byte this[int index]
        {
            get
            {
                if(index < 0)
                    index += Size;
                return data[index];
            }
            set
            {
                if(index < 0)
                    index += Size;
                data[index] = value;
            }
        }

        public void Read(Connection conn) => conn.ReadBytes(data);
        public void Write(Connection conn) => conn.WriteBytes(data);

        public override bool Equals(object obj)
        {
            if(ReferenceEquals(null, obj)) return false;
            if(ReferenceEquals(this, obj)) return true;
            return obj is EncryptedBit bit && Equals(bit);
        }

        private bool Equals(EncryptedBit other) => data.EqualsArr(other.data);
        public override int GetHashCode() => data.GetHashCodeArr();

        public static implicit operator byte[](EncryptedBit bit) => bit?.data;

        public static bool operator ==(EncryptedBit b1, EncryptedBit b2) => b1?.Equals(b2) ?? ReferenceEquals(b2, null);
        public static bool operator !=(EncryptedBit b1, EncryptedBit b2) => !(b1 == b2);
    }
}
