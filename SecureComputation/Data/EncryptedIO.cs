﻿using System;
using System.Collections.Generic;
using SecureComputation.Components.Wires;
using SecureComputation.Utils;

namespace SecureComputation.Data
{
    // ReSharper disable once InconsistentNaming
    public static class EncryptedIO
    {
        public static EncryptedBit[] GetInput(bool[] inputs, List<EncryptedWire> wires)
        {
            if (inputs.Length != wires.Count)
                throw new ArgumentException();
            var eInputs = new EncryptedBit[inputs.Length];
            for (int i = 0; i < inputs.Length; i++)
                eInputs[i] = wires[i][ inputs[i] ];
            return eInputs;
        }

        public static bool[] GetOutput(EncryptedBit[] eOutputs, List<EncryptedWire> wires)
        {
            if (eOutputs.Length != wires.Count)
                throw new ArgumentException();
            var outputs = new bool[eOutputs.Length];
            for (int i = 0; i < eOutputs.Length; i++)
                outputs[i] = wires[i][ eOutputs[i] ];
            return outputs;
        }

        public static byte[] Lock(Config config, EncryptedBit[] eInputs, Aes aes)
        {
            var size = config.VarSize + 1;
            var buffer = new byte[eInputs.Length * size];
            for (int i = 0; i < eInputs.Length; i++)
            {
                buffer.Copy(
                    eInputs[i],
                    dstOff: i * size
                );
            }
            return aes.Encrypt(buffer);
        }

        public static EncryptedBit[] Unlock(Config config, byte[] lockedInput, Aes aes)
        {
            var size = config.VarSize + 1;
            var buffer = aes.Decrypt(lockedInput);
            var eInputs = new EncryptedBit[buffer.Length / size];
            for(int i = 0; i < eInputs.Length; i++)
            {
                eInputs[i] = new EncryptedBit(config);
                ((byte[])eInputs[i]).Copy(
                    buffer,
                    srcOff: i * size
                );
            }
            return eInputs;
        }
    }
}
