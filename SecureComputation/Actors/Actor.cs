using System;
using SecureComputation.Components.Circuits;
using SecureComputation.Utils;

namespace SecureComputation.Actors
{
    /// <summary>
    /// An actor is a participant for secure computation.
    /// </summary>
    public abstract class Actor
    {
        public readonly Connection Connection;
        public readonly Config Config;

        /// <summary>
        /// Controls the verbosity of the output.
        /// Default is no output.
        /// True for a printout of the stages of computation.
        /// </summary>
        public bool Verbose = false;

        public abstract string Name{ get; }

        protected Actor(Connection connection, Config config=null)
        {
            Connection = connection;
            Config = config ?? Config.Default;
        }

        /// <summary>
        /// Executes the given circuit across the connection using the given input.
        /// </summary>
        /// <param name="circuit">The circuit to compute.</param>
        /// <param name="input">The input for this actor to the circuit.</param>
        /// <returns>The output for this actor from the circuit.</returns>
        public abstract bool[] Execute(Circuit circuit, bool[] input);

        protected void Log(string message)
        {
            if(Verbose)
                Console.WriteLine($"[{Name}] {message}");
        }

        /// <summary>
        /// Performs the handshake, which ensures the participants have the same configuration.
        /// </summary>
        protected void Handshake()
        {
            Connection.WriteInt(Config.VarSize);
            if (Config.VarSize != Connection.ReadInt())
                throw new InvalidOperationException("[SecureComputation] Cannot compute with partner. VarSize's are not equal.");
            Connection.WriteInt(Config.SwitchSize);
            if (Config.SwitchSize != Connection.ReadInt())
                throw new InvalidOperationException("[SecureComputation] Cannot compute with partner. SwitchSize's are not equal.");
            Log("Handshake complete.");
        }

        /// <summary>
        /// Data for a single encrypted circuit.
        /// </summary>
        protected struct CircuitData
        {
            /// <summary>
            /// The encrypted circuit itself.
            /// </summary>
            public EncryptedCircuit EncryptedCircuit;

            /// <summary>
            /// The unlock secret for the locked input values.
            /// </summary>
            public Aes Aes;

            /// <summary>
            /// The locked (encrypted) garbled inputs from Bob.
            /// </summary>
            public byte[] LockedInputs;
        }
    }
}