using System;
using SecureComputation.Components.Circuits;
using SecureComputation.Data;
using SecureComputation.Utils;

namespace SecureComputation.Actors
{
    /// <summary>
    /// The Bob actor is the computationally inexpensive actor. He is responsible for making the garbled circuit computed
    /// by Alice.
    /// </summary>
    public class Bob : Actor
    {
        public override string Name => "Bob";

        public Bob(Connection connection, Config config=null) : base(connection, config) { }

        public override bool[] Execute(Circuit circuit, bool[] input)
        {
            Handshake();

            //construct many encrypted versions of the circuit
            var circuits = ConstructCircuits(circuit, input);

            //serve Alice the circuit options
            ServeCircuits(circuits);

            //get the circuit Alice would like to use
            int selected = Connection.ReadInt();

            //reveal all the circuits except the one Alice wants to use so she can verify them
            RevealCircuits(circuits, selected);
            //unlock my inputs for her to use in computation
            UnlockInputs(circuits, selected);

            EncryptedCircuit encryptedCircuit = circuits[selected].EncryptedCircuit;

            //serve Alice her inputs via oblivious transfer
            ServeInputs(encryptedCircuit);

            //Alice computes the circuit

            //Receive my outputs from Alice
            var outputs = ReceiveOutputs(encryptedCircuit);

            return outputs;
        }

        private CircuitData[] ConstructCircuits(Circuit circuit, bool[] input)
        {
            //construct many different encrypted versions of the circuit
            var circuits = new CircuitData[Config.SwitchSize];
            for (int i = 0; i < circuits.Length; i++)
            {
                //encrypt the circuit
                EncryptedCircuit encryptedCircuit = EncryptedCircuit.Encrypt(circuit);
                //find the garbled values for my input
                Aes aes = new Aes();
                var eInputs = EncryptedIO.GetInput(input, encryptedCircuit.BobInputs);
                //lock the input so it cannot be read by alice but also gives her a guarantee that I didn't change it
                //    after her circuit selection
                var lInputs = EncryptedIO.Lock(Config, eInputs, aes);
                circuits[i] = new CircuitData
                {
                    EncryptedCircuit = encryptedCircuit,
                    Aes = aes,
                    LockedInputs = lInputs
                };
            }
            Log("Constructed encrypted circuits.");
            return circuits;
        }

        private void ServeCircuits(CircuitData[] circuits)
        {
            foreach(var data in circuits)
            {
                //write the circuit and the input commitment
                Connection.Write(data.EncryptedCircuit);
                Connection.WriteByteArr(data.LockedInputs);
            }
        }

        private void RevealCircuits(CircuitData[] circuits, int exclude)
        {
            //reveal all circuits *except* the one that Alice wanted to use
            for(int i = 0; i < circuits.Length; i++)
            {
                if (i == exclude)
                    continue;
                Connection.Write(circuits[i].EncryptedCircuit.Secrets);
            }
            Log("Revealed circuit secrets.");
        }

        private void UnlockInputs(CircuitData[] circuits, int selected)
        {
            //reveal the unlock code for my input
            Connection.Write(circuits[selected].Aes);
            Log("Revealed inputs.");
        }

        private void ServeInputs(EncryptedCircuit circuit)
        {
            //serve alice her inputs through oblivious transfer
            ObliviousTransfer transfer = new ObliviousTransfer(Connection, true, Config.VarSize + 1);
            foreach(var encryptedWire in circuit.AliceInputs)
                transfer.Serve(encryptedWire);
            Log("Served Alice's inputs.");
        }

        private bool[] ReceiveOutputs(EncryptedCircuit circuit)
        {
            //read the output from Alice
            EncryptedBit bit = new EncryptedBit(Config);
            var outputs = new bool[circuit.BobOutputs.Count];
            try
            {
                for (int i = 0; i < outputs.Length; i++)
                {
                    Connection.Read(bit);
                    outputs[i] = circuit.BobOutputs[i][ bit ];
                }
            }catch(ArgumentException)
            {
                throw new InvalidOperationException("[Bob] Alice tried to cheat!");
            }
            Log("Received outputs.");
            return outputs;
        }
    }
}