using System;
using SecureComputation.Components.Circuits;
using SecureComputation.Data;
using SecureComputation.Utils;
using Random = SecureComputation.Utils.Random;

namespace SecureComputation.Actors
{
    /// <summary>
    /// The Alice actor is the computationally expensive actor. She is the actor actually responsible for computing the
    /// garbled circuit given to her by Bob.
    /// </summary>
    public class Alice : Actor
    {
        public override string Name => "Alice";

        public Alice(Connection connection, Config config=null) : base(connection, config) { }

        public override bool[] Execute(Circuit circuit, bool[] input)
        {
            Handshake();

            var circuits = ReceiveCircuits();

            //generate a random selection from the options
            int selected = ChooseSelection();
            Connection.WriteInt(selected);

            //verify the other circuits
            VerifyCircuits(circuit, circuits, selected);

            EncryptedCircuit encryptedCircuit = circuits[selected].EncryptedCircuit;

            //unlock Bob's inputs for the selection
            var bobInput = UnlockInputs(circuits, selected);
            //encrypt my inputs
            var aliceInput = ReceiveInputs(encryptedCircuit, input);

            //execute the circuit
            var (aliceOutput, bobOutput) = encryptedCircuit.Execute(aliceInput, bobInput);
            Log("Executed circuit.");

            //send bob his outputs
            ServeOutputs(bobOutput);

            //decrypt my outputs
            var outputs = DecryptOutputs(encryptedCircuit, aliceOutput);

            return outputs;
        }

        private CircuitData[] ReceiveCircuits()
        {
            //receive various encrypted versions of the circuit
            var circuits = new CircuitData[Config.SwitchSize];
            for(int i = 0; i < circuits.Length; i++)
            {
                circuits[i] = new CircuitData
                {
                    EncryptedCircuit = new EncryptedCircuit(Config, Connection),
                    Aes = null,
                    LockedInputs = Connection.ReadByteArr()
                };
            }
            Log("Received circuits.");
            return circuits;
        }

        private int ChooseSelection()
        {
            int selected = Random.GetInt(Config.SwitchSize);
            Log("Choose selected.");
            return selected;
        }

        private void VerifyCircuits(Circuit circuit, CircuitData[] circuits, int selected)
        {
            for (int i = 0; i < circuits.Length; i++)
            {
                if(i == selected)
                    continue;
                Connection.Read(circuits[i].EncryptedCircuit.Secrets);
                Circuit decrypted = EncryptedCircuit.Decrypt(circuits[i].EncryptedCircuit);
                if (decrypted != circuit)
                    throw new InvalidOperationException("[Alice] Bob tried to cheat!");
            }
            Log("Verified other circuits.");
        }

        private EncryptedBit[] UnlockInputs(CircuitData[] circuits, int selected)
        {
            //get the unlock code for bob's inputs
            circuits[selected].Aes = new Aes(Connection);
            var bobInput = EncryptedIO.Unlock(Config, circuits[selected].LockedInputs, circuits[selected].Aes);
            Log("Unlocked Bob's input.");
            return bobInput;
        }

        private EncryptedBit[] ReceiveInputs(EncryptedCircuit encryptedCircuit, bool[] input)
        {
            var aliceInput = new EncryptedBit[encryptedCircuit.AliceInputs.Count];
            //garble my inputs from Bob's oblivious transfer
            ObliviousTransfer otrans = new ObliviousTransfer(Connection, false, Config.VarSize + 1);
            for (int i = 0; i < aliceInput.Length; i++)
                aliceInput[i] = new EncryptedBit(Config, otrans.Receive(input[i] ? 1 : 0));

            Log("Encrypted inputs.");
            return aliceInput;
        }

        private void ServeOutputs(EncryptedBit[] bobOutput)
        {
            foreach (EncryptedBit bit in bobOutput)
                Connection.Write(bit);
            Log("Sent Bob's outputs.");
        }

        private bool[] DecryptOutputs(EncryptedCircuit encryptedCircuit, EncryptedBit[] aliceOutput)
        {
            var outputs = new bool[encryptedCircuit.AliceOutputs.Count];
            for (int i = 0; i < aliceOutput.Length; i++)
            {
                outputs[i] = encryptedCircuit.AliceOutputs[i][ aliceOutput[i] ];
            }
            Log("Decrypted outputs.");
            return outputs;
        }
    }
}