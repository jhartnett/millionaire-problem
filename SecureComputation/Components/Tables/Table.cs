﻿using System;
using System.Collections.Generic;
using SecureComputation.Components.Circuits;
using SecureComputation.Utils;

namespace SecureComputation.Components.Tables
{
    public class Table : TableBase<Circuit, bool, bool>
    {
        public Table(Circuit circuit, int id, int length)
            : base(circuit, id, length)
        { }

        public Table(Circuit circuit,  Connection conn)
            : base(circuit, conn)
        { }

        public void Fill(Func<bool[], bool> func)
        {
            int i = 0;
            foreach (var inputs in GetInputs(Length))
                Entries[i++] = func(inputs);
        }

        public override bool GetEntry(IEnumerable<bool> inputs)
        {
            int index = 0;
            foreach (bool bit in inputs)
                index = (index << 1) | (bit ? 1 : 0);
            return Entries[index];
        }

        public override void Read(Connection conn)
        {
            base.Read(conn);
            conn.ReadBools(Entries);
        }

        public override void Write(Connection conn)
        {
            base.Write(conn);
            conn.WriteBools(Entries);
        }
    }
}
