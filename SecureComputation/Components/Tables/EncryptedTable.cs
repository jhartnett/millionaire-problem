﻿using System;
using System.Collections.Generic;
using System.Linq;
using SecureComputation.Components.Circuits;
using SecureComputation.Components.Wires;
using SecureComputation.Data;
using SecureComputation.Utils;

namespace SecureComputation.Components.Tables
{
    public class EncryptedTable : TableBase<EncryptedCircuit, EncryptedEntry, EncryptedBit>
    {
        public static void Encrypt(Table table, EncryptedTable eTable, EncryptedWire[] inWires, EncryptedWire outWire)
        {
            Crypt(eTable.Encrypt, table.Entries, eTable.Entries, inWires, outWire);
            Permute(eTable.Entries, inWires);
        }

        public static void Decrypt(EncryptedTable eTable, Table table, EncryptedWire[] inWires, EncryptedWire outWire)
        {
            var buffer = eTable.Entries.Copy();
            Permute(buffer, inWires);
            Crypt(eTable.Decrypt, buffer, table.Entries, inWires, outWire);
        }

        private static void Crypt<I, O>(Func<I, EncryptedWire, EncryptedBit[], O> cryptor, I[] iEntries, O[] oEntries, EncryptedWire[] inWires, EncryptedWire outWire)
        {
            if(iEntries.Length != (1 << inWires.Length) || oEntries.Length != iEntries.Length)
                throw new ArgumentException();
            int length = inWires.Length;
            int index = 0;
            foreach(var inputs in GetInputs(length))
            {
                var einputs = new EncryptedBit[length];

                for (int i = 0; i < length; i++)
                    einputs[i] = inWires[i][inputs[i]];

                oEntries[index] = cryptor(iEntries[index], outWire, einputs);

                index++;
            }
        }
        private static void Permute<E>(E[] entries, EncryptedWire[] inWires)
        {
            if (entries.Length != (1 << inWires.Length))
                throw new ArgumentException();
            int length = inWires.Length;
            for(int i = 0; i < length; i++)
            {
                if (!inWires[i].Permutation)
                    continue;
                int gCount = 1 << i;
                int gSize = 1 << (length - 1 - i);
                for(int j = 0; j < gCount; j++)
                {
                    for(int k = 0; k < gSize; k++)
                    {
                        Util.Exchg(
                            ref entries[j * gSize * 2 + k],
                            ref entries[j * gSize * 2 + k + gSize]
                        );
                    }
                }
            }
        }

        public EncryptedTable(EncryptedCircuit circuit, int id, int length)
            : base(circuit, id, length)
        { }

        public EncryptedTable(EncryptedCircuit circuit, Connection conn)
            : base(circuit, conn)
        { }

        public override void Read(Connection conn)
        {
            base.Read(conn);
            for (int i = 0; i < Entries.Length; i++)
                Entries[i] = new EncryptedEntry(Circuit.Config);
            conn.Reads(Entries);
        }

        public override void Write(Connection conn)
        {
            base.Write(conn);
            conn.Writes(Entries);
        }

        public override EncryptedBit GetEntry(IEnumerable<EncryptedBit> inputs)
        {
            EncryptedBit[] ins = inputs.ToArray();
            int index = 0;
            foreach (EncryptedBit bit in ins)
                index = (index << 1) | bit[-1];
            return Decrypt(Entries[index], ins);
        }

        private EncryptedEntry Encrypt(bool output, EncryptedWire outWire, EncryptedBit[] einputs)
        {
            return Encrypt(outWire[output], einputs);
        }

        private bool Decrypt(EncryptedEntry entry, EncryptedWire outWire, EncryptedBit[] einputs)
        {
            return outWire[ Decrypt(entry, einputs) ];
        }

        private EncryptedEntry Encrypt(EncryptedBit eoutput, EncryptedBit[] einputs)
        {
            EncryptedEntry entry = new EncryptedEntry(Circuit.Config, eoutput);
            Crypt(entry, einputs);
            return entry;
        }

        private EncryptedBit Decrypt(EncryptedEntry entry, EncryptedBit[] einputs)
        {
            EncryptedEntry buffer = new EncryptedEntry(Circuit.Config, entry);
            Crypt(buffer, einputs);
            return new EncryptedBit(Circuit.Config, buffer);
        }

        private void Crypt(EncryptedEntry entry, EncryptedBit[] einputs)
        {
            foreach(var key in GenerateKeys(einputs))
            {
                ((byte[])entry).XorEquals(
                    Circuit.Config.Hash.ComputeHash(key)
                );
            }
        }

        private IEnumerable<byte[]> GenerateKeys(EncryptedBit[] einputs)
        {
            byte[] key = GenerateSuperKey(einputs);
            for (int i = 0; i < Length; i++)
            {
                key.Copy(einputs[i], Circuit.Config.VarSize);
                yield return key;
            }
        }

        private byte[] GenerateSuperKey(EncryptedBit[] einputs)
        {
            var key = new byte[Circuit.Config.VarSize + Length + 4];
            Set(key, Circuit.Config.VarSize, ID);
            for (int i = 0; i < Length; i++)
            {
                key[Circuit.Config.VarSize + 4 + i] = einputs[i][-1];
            }
            return key;
        }

        private static void Set(byte[] arr, int index, int value)
        {
            for (int i = 0; i < 4; i++, value >>= 8)
                arr[index++] = (byte)(value & 0xFF);
        }
    }
}
