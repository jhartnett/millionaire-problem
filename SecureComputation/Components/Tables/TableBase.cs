﻿using System.Collections.Generic;
using SecureComputation.Components.Circuits;
using SecureComputation.Utils;

namespace SecureComputation.Components.Tables
{
    public abstract class TableBase<C, E, B> : Component<C> where C : ICircuit
    {
        public static IEnumerable<bool[]> GetInputs(int length)
        {
            var buffer = new bool[length];
            while (true)
            {
                yield return buffer;
                int index = buffer.Length - 1;
                while (index >= 0 && buffer[index])
                    buffer[index--] = false;
                if (index < 0)
                    break;
                buffer[index] = true;
            }
        }

        public int Length { get; protected set; }
        protected internal E[] Entries;

        protected internal TableBase(C circuit, int id, int length)
            : base(circuit, id)
        {
            Length = length;
            Entries = new E[1 << length];
        }

        protected internal TableBase(C circuit, Connection conn)
            : base(circuit, conn)
        { }

        public abstract B GetEntry(IEnumerable<B> inputs);

        public B this[params B[] inputs] => GetEntry(inputs);

        public override void Read(Connection conn)
        {
            base.Read(conn);
            Length = conn.ReadInt();
            Entries = new E[1 << Length];
        }

        public override void Write(Connection conn)
        {
            base.Write(conn);
            conn.WriteInt(Length);
        }
    }
}
