﻿using System;
using System.Collections.Generic;
using System.Linq;
using SecureComputation.Components.Gates;
using SecureComputation.Components.Wires;
using SecureComputation.Data;
using SecureComputation.Utils;
// ReSharper disable CoVariantArrayConversion

namespace SecureComputation.Components.Circuits
{
    public class EncryptedCircuit : CircuitBase<EncryptedBit, EncryptedWire, EncryptedGate>, ISerializable
    {
        public static EncryptedCircuit Encrypt(Circuit circuit)
        {
            EncryptedCircuit eCircuit = new EncryptedCircuit(circuit.Config);
            foreach (Wire wire in circuit.Wires)
            {
                eCircuit.Add(
                    new EncryptedWire(eCircuit, wire.ID)
                );
            }
            foreach (Gate gate in circuit.Gates)
            {
                EncryptedGate egate = new EncryptedGate(eCircuit, gate.ID, eCircuit.Translate(gate.Inputs), eCircuit.Translate(gate.Output));
                EncryptedGate.Encrypt(gate, egate);
                eCircuit.Add(egate);
            }
            eCircuit.AliceInputs.AddRange( eCircuit.Translate(circuit.AliceInputs.ToArray()) );
            eCircuit.BobInputs.AddRange( eCircuit.Translate(circuit.BobInputs.ToArray()) );
            eCircuit.AliceOutputs.AddRange( eCircuit.Translate(circuit.AliceOutputs.ToArray()) );
            eCircuit.BobOutputs.AddRange( eCircuit.Translate(circuit.BobOutputs.ToArray()) );
            return eCircuit;
        }
        public static Circuit Decrypt(EncryptedCircuit eCircuit)
        {
            Circuit circuit = new Circuit(eCircuit.Config);
            foreach(EncryptedWire ewire in eCircuit.Wires)
            {
                circuit.Add(
                    new Wire(circuit, ewire.ID)
                );
            }
            foreach(EncryptedGate egate in eCircuit.Gates)
            {
                Gate gate = new Gate(circuit, egate.ID, circuit.Translate(egate.Inputs), circuit.Translate(egate.Output));
                EncryptedGate.Decrypt(egate, gate);
                circuit.Add(gate);
            }
            circuit.AliceInputs.AddRange( circuit.Translate(eCircuit.AliceInputs.ToArray()) );
            circuit.BobInputs.AddRange( circuit.Translate(eCircuit.BobInputs.ToArray()) );
            circuit.AliceOutputs.AddRange( circuit.Translate(eCircuit.AliceOutputs.ToArray()) );
            circuit.BobOutputs.AddRange( circuit.Translate(eCircuit.BobOutputs.ToArray()) );
            return circuit;
        }

        public readonly EncryptionSecrets Secrets;

        public EncryptedCircuit(Config config) : base(config)
        {
            Secrets = new EncryptionSecrets(this);
        }

        public EncryptedCircuit(Config config, Connection conn)
            : this(config)
        {
            Read(conn);
        }

        public override object GetWire()
        {
            EncryptedWire wire = new EncryptedWire(this, GetWireID());
            Add(wire);
            return wire;
        }

        public override object GetGate(Func<bool[], bool> func, params object[] wires)
        {
            throw new NotSupportedException();
        }

        public override (EncryptedBit[], EncryptedBit[]) Execute(EncryptedBit[] aliceIn, EncryptedBit[] bobIn)
        {
            var values = new EncryptedBit[Wires.Count];

            for (int i = 0; i < AliceInputs.Count; i++)
                values[AliceInputs[i].ID - 1] = aliceIn[i];
            for (int i = 0; i < BobInputs.Count; i++)
                values[BobInputs[i].ID - 1] = bobIn[i];

            foreach (EncryptedGate gate in Gates)
                values[gate.Output.ID - 1] = gate.Execute(values);

            var aliceOut = new EncryptedBit[AliceOutputs.Count];
            var bobOut = new EncryptedBit[BobOutputs.Count];
            for (int i = 0; i < AliceOutputs.Count; i++)
                aliceOut[i] = values[ AliceOutputs[i].ID - 1 ];
            for (int i = 0; i < BobOutputs.Count; i++)
                bobOut[i] = values[ BobOutputs[i].ID - 1 ];
            return (aliceOut, bobOut);
        }

        public void Read(Connection conn)
        {
            Wires = conn.ReadArr(() => new EncryptedWire(this, conn)).ToList();
            Gates = conn.ReadArr(() => new EncryptedGate(this, conn)).ToList();

            AliceInputs = ReadIOList(conn);
            BobInputs = ReadIOList(conn);
            AliceOutputs = ReadIOList(conn);
            BobOutputs = ReadIOList(conn);

            foreach (EncryptedWire wire in AliceOutputs)
                wire.ReadSecrets(conn);
        }

        public void Write(Connection conn)
        {
            conn.WriteArr(Wires.ToArray());
            conn.WriteArr(Gates.ToArray());

            WriteIOList(conn, AliceInputs);
            WriteIOList(conn, BobInputs);
            WriteIOList(conn, AliceOutputs);
            WriteIOList(conn, BobOutputs);

            foreach (EncryptedWire wire in AliceOutputs)
                wire.WriteSecrets(conn);
        }

        private List<EncryptedWire> ReadIOList(Connection conn)
        {
            int count = conn.ReadInt();
            var wires = new List<EncryptedWire>();
            for(int i = 0; i < count; i++)
            {
                wires.Add(
                    GetWireById(conn.ReadInt()) as EncryptedWire
                );
            }
            return wires;
        }

        private void WriteIOList(Connection conn, List<EncryptedWire> wires)
        {
            conn.WriteInt(wires.Count);
            foreach(EncryptedWire wire in wires)
                conn.WriteInt(wire.ID);
        }

        public class EncryptionSecrets : ISerializable
        {
            private readonly EncryptedCircuit eCircuit;

            public EncryptionSecrets(EncryptedCircuit eCircuit)
            {
                this.eCircuit = eCircuit;
            }

            public void Read(Connection conn)
            {
                foreach (EncryptedWire wire in eCircuit.Wires)
                    wire.ReadSecrets(conn);
            }

            public void Write(Connection conn)
            {
                foreach (EncryptedWire wire in eCircuit.Wires)
                    wire.WriteSecrets(conn);
            }
        }
    }
}
