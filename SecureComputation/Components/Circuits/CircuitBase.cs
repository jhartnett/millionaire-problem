﻿using System;
using System.Collections.Generic;
using SecureComputation.Components.Wires;

namespace SecureComputation.Components.Circuits
{
    public interface ICircuit
    {
        object GetWire();

        object GetWireById(int id);

        object GetGate(Func<bool[], bool> func, params object[] wires);
    }

    public abstract class CircuitBase<B, W, G> : ICircuit
    {
        public readonly Config Config;

        protected internal List<W> Wires = new List<W>();
        protected internal List<G> Gates = new List<G>();

        public List<W> AliceInputs = new List<W>();
        public List<W> AliceOutputs = new List<W>();

        public List<W> BobInputs = new List<W>();
        public List<W> BobOutputs = new List<W>();

        protected CircuitBase(Config config)
        {
            Config = config;
        }

        public abstract object GetWire();
        public abstract object GetGate(Func<bool[], bool> func, params object[] wires);
        public abstract (B[] aliceOut, B[] bobOut) Execute(B[] aliceIn, B[] bobIn);

        public object GetWireById(int id)
        {
            return Wires[id - 1];
        }

        protected internal int GetWireID()
        {
            return Wires.Count + 1;
        }
        protected internal int GetGateID()
        {
            return Gates.Count + 1;
        }
        protected internal W Add(W wire)
        {
            Wires.Add(wire);
            return wire;
        }
        protected internal G Add(G gate)
        {
            Gates.Add(gate);
            return gate;
        }

        public W Translate<C>(WireBase<C> wire) where C : ICircuit
        {
            return (W)GetWireById(wire.ID);
        }

        public W[] Translate<C>(params WireBase<C>[] wires) where C : ICircuit
        {
            var ewires = new W[wires.Length];
            for (int i = 0; i < wires.Length; i++)
                ewires[i] = (W)GetWireById(wires[i].ID);
            return ewires;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as CircuitBase<B, W, G>);
        }

        protected bool Equals(CircuitBase<B, W, G> cir)
        {
            if (ReferenceEquals(cir, null))
                return false;

            return Compare(Wires, cir.Wires) &&
                Compare(Gates, cir.Gates) &&
                Compare(AliceInputs, cir.AliceInputs) &&
                Compare(AliceOutputs, cir.AliceOutputs) &&
                Compare(BobInputs, cir.BobInputs) &&
                Compare(BobOutputs, cir.BobOutputs);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (Wires != null ? Wires.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Gates != null ? Gates.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (AliceInputs != null ? AliceInputs.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (AliceOutputs != null ? AliceOutputs.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (BobInputs != null ? BobInputs.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (BobOutputs != null ? BobOutputs.GetHashCode() : 0);
                return hashCode;
            }
        }

        private static bool Compare<E>(List<E> l1, List<E> l2)
        {
            if (l1.Count != l2.Count)
                return false;
            for(int i = 0; i < l1.Count; i++)
            {
                if (!l1[i].Equals(l2[i]))
                    return false;
            }
            return true;
        }

        public static bool operator ==(CircuitBase<B, W, G> c1, CircuitBase<B, W, G> c2)
            => c1?.Equals(c2) ?? ReferenceEquals(c2, null);
        public static bool operator !=(CircuitBase<B, W, G> c1, CircuitBase<B, W, G> c2)
            => !(c1 == c2);
    }
}
