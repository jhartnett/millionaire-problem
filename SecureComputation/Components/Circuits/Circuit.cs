﻿using System;
using System.Collections.Generic;
using System.Linq;
using SecureComputation.Components.Gates;
using SecureComputation.Components.Wires;
using SecureComputation.Utils;

namespace SecureComputation.Components.Circuits
{
    public class Circuit : CircuitBase<bool, Wire, Gate>, ISerializable
    {
        public Circuit(Config config) : base(config) { }

        public Circuit(Config config, Connection conn)
            : this(config)
        {
            Read(conn);
        }

        public override object GetWire()
        {
            Wire wire = new Wire(this, GetWireID());
            Add(wire);
            return wire;
        }

        public override object GetGate(Func<bool[], bool> func, params object[] wires)
        {
            Gate gate = new Gate(this, GetGateID(), func, wires.Cast<Wire>().ToArray());
            Add(gate);
            return gate.Output;
        }

        public override (bool[], bool[]) Execute(bool[] aliceIn, bool[] bobIn)
        {
            var values = new bool[Wires.Count];

            for (int i = 0; i < AliceInputs.Count; i++)
                values[ AliceInputs[i].ID - 1 ] = aliceIn[i];
            for (int i = 0; i < BobInputs.Count; i++)
                values[ BobInputs[i].ID - 1 ] = bobIn[i];

            foreach(Gate gate in Gates)
                values[ gate.Output.ID - 1 ] = gate.Execute(values);

            var aliceOut = new bool[AliceOutputs.Count];
            var bobOut = new bool[BobOutputs.Count];
            for (int i = 0; i < AliceOutputs.Count; i++)
                aliceOut[i] = values[ AliceOutputs[i].ID - 1];
            for (int i = 0; i < BobOutputs.Count; i++)
                bobOut[i] = values[ BobOutputs[i].ID - 1 ];
            return (aliceOut, bobOut);
        }

        public void Read(Connection conn)
        {
            Wires = conn.ReadArr(() => new Wire(this, conn)).ToList();
            Gates = conn.ReadArr(() => new Gate(this, conn)).ToList();

            AliceInputs = ReadIOList(conn);
            BobInputs = ReadIOList(conn);
            AliceOutputs = ReadIOList(conn);
            BobOutputs = ReadIOList(conn);
        }

        public void Write(Connection conn)
        {
            conn.WriteArr(Wires.ToArray());
            conn.WriteArr(Gates.ToArray());

            WriteIOList(conn, AliceInputs);
            WriteIOList(conn, BobInputs);
            WriteIOList(conn, AliceOutputs);
            WriteIOList(conn, BobOutputs);
        }

        private List<Wire> ReadIOList(Connection conn)
        {
            int count = conn.ReadInt();
            var wires = new List<Wire>();
            for (int i = 0; i < count; i++)
                wires.Add(
                    GetWireById(conn.ReadInt()) as Wire
                );
            return wires;
        }

        private void WriteIOList(Connection conn, List<Wire> wires)
        {
            conn.WriteInt(wires.Count);
            foreach(Wire wire in wires)
                conn.WriteInt(wire.ID);
        }

        public Wire AliceInput()
        {
            Wire wire = GetWire() as Wire;
            AliceInputs.Add(wire);
            return wire;
        }
        public Wire BobInput()
        {
            Wire wire = GetWire() as Wire;
            BobInputs.Add(wire);
            return wire;
        }
        public Word AliceInput(int length)
        {
            return InputWord(AliceInput, length);
        }
        public Word BobInput(int length)
        {
            return InputWord(BobInput, length);
        }

        private static Word InputWord(Func<Wire> wireSrc, int length)
        {
            var wires = new Wire[length];
            for (int i = 0; i < wires.Length; i++)
                wires[i] = wireSrc();
            return new Word(wires);
        }

        private Wire Purge(Wire input)
        {
            if(input is InvertedWire iWire)
                input = GetGate(arr => !arr[0], iWire.Source) as Wire;
            return input;
        }

        public void AliceOutput(Wire wire)
        {
            wire = Purge(wire);
            AliceOutputs.Add(wire);
        }
        public void BobOutput(Wire wire)
        {
            wire = Purge(wire);
            BobOutputs.Add(wire);
        }
        public void AliceOutput(Word word)
        {
            for (int i = 0; i < word.Length; i++)
                AliceOutput(word[i]);
        }
        public void BobOutput(Word word)
        {
            for (int i = 0; i < word.Length; i++)
                BobOutput(word[i]);
        }
    }
}
