﻿using SecureComputation.Components.Circuits;
using SecureComputation.Utils;

namespace SecureComputation.Components
{
    public abstract class Component<C> : IIdentifiable, ISerializable where C : ICircuit
    {
        internal readonly C Circuit;
        public int ID { get; private set; }

        protected internal Component(C circuit, int id)
        {
            Circuit = circuit;
            ID = id;
        }

        protected internal Component(C circuit, Connection conn)
        {
            Circuit = circuit;
            // ReSharper disable once VirtualMemberCallInConstructor
            Read(conn);
        }

        public override bool Equals(object obj)
        {
            if(ReferenceEquals(null, obj)) return false;
            if(ReferenceEquals(this, obj)) return true;
            return GetType() == obj.GetType() && Equals((Component<C>)obj);
        }
        protected virtual bool Equals(Component<C> other) => ID == other.ID;
        // ReSharper disable once NonReadonlyMemberInGetHashCode
        public override int GetHashCode() => ID;

        public static bool operator ==(Component<C> comp1, Component<C> comp2) => comp1?.Equals(comp2) ?? ReferenceEquals(comp2, null);
        public static bool operator !=(Component<C> comp1, Component<C> comp2) => !(comp1 == comp2);

        public virtual void Read(Connection conn) => ID = conn.ReadInt();
        public virtual void Write(Connection conn) => conn.WriteInt(ID);
    }
}
