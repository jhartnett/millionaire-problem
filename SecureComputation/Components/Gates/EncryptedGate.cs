﻿using System.Collections.Generic;
using SecureComputation.Components.Circuits;
using SecureComputation.Components.Tables;
using SecureComputation.Components.Wires;
using SecureComputation.Data;
using SecureComputation.Utils;

namespace SecureComputation.Components.Gates
{
    public class EncryptedGate : GateBase<EncryptedCircuit, EncryptedWire, EncryptedBit>
    {
        public static void Encrypt(Gate gate, EncryptedGate eGate)
        {
            EncryptedTable.Encrypt(gate.Table, eGate.Table, eGate.Inputs, eGate.Output);
        }
        public static void Decrypt(EncryptedGate eGate, Gate gate)
        {
            EncryptedTable.Decrypt(eGate.Table, gate.Table, eGate.Inputs, eGate.Output);
        }

        public EncryptedTable Table;

        public EncryptedGate(EncryptedCircuit circuit, int id, EncryptedWire[] inputs)
            : this(circuit, id, inputs, circuit.GetWire() as EncryptedWire)
        { }

        public EncryptedGate(EncryptedCircuit circuit, int id, EncryptedWire[] inputs, EncryptedWire output)
            : base(circuit, id, inputs, output)
        {
            Table = new EncryptedTable(circuit, id, inputs.Length);
        }

        public EncryptedGate(EncryptedCircuit circuit, Connection conn)
            : base(circuit, conn)
        { }

        public override EncryptedBit GetEntry(IEnumerable<EncryptedBit> inputs) => Table.GetEntry(inputs);

        public override void Read(Connection conn)
        {
            base.Read(conn);
            Table = new EncryptedTable(Circuit, conn);
        }

        public override void Write(Connection conn)
        {
            base.Write(conn);
            conn.Write(Table);
        }
    }
}
