﻿using System.Collections.Generic;
using System.Linq;
using SecureComputation.Components.Circuits;
using SecureComputation.Components.Wires;
using SecureComputation.Utils;

namespace SecureComputation.Components.Gates
{
    public abstract class GateBase<C, W, B> : Component<C> where W : WireBase<C> where C : ICircuit
    {
        public W[] Inputs { get; protected set; }
        public W Output { get; protected set; }

        protected internal GateBase(C circuit, int id, W[] inputs, W output)
            : base(circuit, id)
        {
            Inputs = inputs;
            Output = output;
        }

        protected internal GateBase(C circuit, Connection conn)
            : base(circuit, conn)
        { }

        public abstract B GetEntry(IEnumerable<B> inputs);

        public B this[params B[] inputs] => GetEntry(inputs);

        public B Execute(B[] index)
        {
            return GetEntry(
                Inputs.Select(
                    input => index[ input.ID - 1 ]
                )
            );
        }

        public override void Read(Connection conn)
        {
            base.Read(conn);

            Inputs = new W[conn.ReadInt()];
            for (int i = 0; i < Inputs.Length; i++)
                Inputs[i] = Circuit.GetWireById( conn.ReadInt() ) as W;

            Output = Circuit.GetWireById( conn.ReadInt() ) as W;
        }

        public override void Write(Connection conn)
        {
            base.Write(conn);

            conn.WriteInt(Inputs.Length);
            foreach (var wire in Inputs)
                conn.WriteInt(wire.ID);

            conn.WriteInt(Output.ID);
        }
    }
}
