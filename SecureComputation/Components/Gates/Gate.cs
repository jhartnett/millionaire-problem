﻿using System;
using System.Collections.Generic;
using SecureComputation.Components.Circuits;
using SecureComputation.Components.Tables;
using SecureComputation.Components.Wires;
using SecureComputation.Utils;

namespace SecureComputation.Components.Gates
{
    public class Gate : GateBase<Circuit, Wire, bool>
    {
        public Table Table;

        public Gate(Circuit circuit, int id, Wire[] inputs)
            : this(circuit, id, inputs, circuit.GetWire() as Wire)
        { }

        public Gate(Circuit circuit, int id, Wire[] inputs, Wire output)
            : base(circuit, id, inputs, output)
        {
            Table = new Table(circuit, id, inputs.Length);
        }

        public Gate(Circuit circuit, Connection conn)
            : base(circuit, conn)
        { }

        public Gate(Circuit circuit, int id, Func<bool[], bool> func, params Wire[] inputs)
            : this(circuit, id, inputs)
        {
            Table.Fill(func);
        }

        public override bool GetEntry(IEnumerable<bool> inputs) => Table.GetEntry(inputs);

        public override void Read(Connection conn)
        {
            base.Read(conn);
            Table = new Table(Circuit, conn);
        }

        public override void Write(Connection conn)
        {
            base.Write(conn);
            conn.Write(Table);
        }
    }
}
