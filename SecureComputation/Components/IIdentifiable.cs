﻿namespace SecureComputation.Components
{
    public interface IIdentifiable
    {
        int ID { get; }
    }
}
