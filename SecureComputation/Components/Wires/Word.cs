﻿using System;

namespace SecureComputation.Components.Wires
{
    public class Word
    {
        private readonly Wire[] wires;

        public int Length => wires.Length;

        public Word(int length)
            : this(new Wire[length])
        { }

        public Word(Wire[] wires)
        {
            this.wires = wires;
        }

        public Wire this[int index]
        {
            get
            {
                if(index < 0)
                    index += Length;
                if (index > Length)
                    return false;
                return wires[index];
            }
            private set
            {
                if(index < 0)
                    index += Length;
                wires[index] = value;
            }
        }

        public Word SignExtend(int length)
        {
            if (length <= Length)
                return this;
            Word word = new Word(length);
            for (int i = 0; i < Length; i++)
                word[i] = this[i];
            for (int i = Length; i < length; i++)
                word[i] = this[-1];
            return word;
        }

        private static Word Convert(ulong bits, int length)
        {
            Word word = new Word(length);
            for (int i = 0; i < length; i++, bits >>= 1)
                word[i] = (bits & 1) == 1;
            return word;
        }
        private static Word Convert(long bits, int length)
        {
            return Convert((ulong)bits, length);
        }

        public static implicit operator Word(bool value)
        {
            return Convert(value ? 1u : 0u, 1);
        }
        public static implicit operator Word(byte value)
        {
            return Convert(value, 8);
        }
        public static implicit operator Word(ushort value)
        {
            return Convert(value, 16);
        }
        public static implicit operator Word(uint value)
        {
            return Convert(value, 32);
        }
        public static implicit operator Word(ulong value)
        {
            return Convert(value, 64);
        }
        public static implicit operator Word(sbyte value)
        {
            return Convert(value, 8);
        }
        public static implicit operator Word(short value)
        {
            return Convert(value, 16);
        }
        public static implicit operator Word(int value)
        {
            return Convert(value, 32);
        }
        public static implicit operator Word(long value)
        {
            return Convert(value, 64);
        }

        public static Word operator ~(Word input)
        {
            Word output = new Word(input.Length);
            for (int i = 0; i < input.Length; i++)
                output[i] = !input[i];
            return output;
        }
        public static Word operator -(Word input)
        {
            return ~input + 1;
        }

        private delegate Wire ArithOpFunc(Wire in1, Wire in2, ref Wire carry);

        private static Word BitOp(Func<Wire, Wire, Wire> op, Word input1, Word input2)
        {
            Word output = new Word(Math.Max(input1.Length, input2.Length));
            for (int i = 0; i < output.Length; i++)
                output[i] = op(input1[i], input2[i]);
            return output;
        }
        private static Word ArithOp(ArithOpFunc op, Word input1, Word input2)
        {
            Word output = new Word(Math.Max(input1.Length, input2.Length));
            Wire carry = false;
            for (int i = 0; i < output.Length; i++)
                output[i] = op(input1[i], input2[i], ref carry);
            return output;
        }

        public static Word operator &(Word input1, Word input2)
        {
            return BitOp((a, b) => a & b, input1, input2);
        }
        public static Word operator |(Word input1, Word input2)
        {
            return BitOp((a, b) => a | b, input1, input2);
        }
        public static Word operator ^(Word input1, Word input2)
        {
            return BitOp((a, b) => a ^ b, input1, input2);
        }

        public static Word operator +(Word input1, Word input2)
        {
            return ArithOp(Wire.Add, input1, input2);
        }
        public static Word operator -(Word input1, Word input2)
        {
            return ArithOp(Wire.Subtract, input1, input2);
        }

        public static Wire operator ==(Word input1, Word input2)
        {
            return !(input1 != input2);
        }
        public static Wire operator !=(Word input1, Word input2)
        {
            Wire output = false;
            for (int i = 0; i < Math.Max(input1.Length, input2.Length); i++)
                output |= input1[i] ^ input2[i];
            return output;
        }

        public static Wire operator <(Word input1, Word input2)
        {
            return (input1 - input2)[-1];
        }
        public static Wire operator >(Word input1, Word input2)
        {
            return input2 < input1;
        }
        public static Wire operator >=(Word input1, Word input2)
        {
            return !(input1 < input2);
        }
        public static Wire operator <=(Word input1, Word input2)
        {
            return !(input1 > input2);
        }
    }
}
