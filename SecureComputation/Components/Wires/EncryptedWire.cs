﻿using System;
using SecureComputation.Components.Circuits;
using SecureComputation.Data;
using SecureComputation.Utils;
using Random = SecureComputation.Utils.Random;

namespace SecureComputation.Components.Wires
{
    public class EncryptedWire : WireBase<EncryptedCircuit>, IObliviousDataSource
    {
        public readonly EncryptedBit Zero;
        public readonly EncryptedBit One;

        internal bool Permutation
        {
            get => Zero[-1] != 0;
            set
            {
                Zero[-1] = (byte)(value ? 1 : 0);
                One[-1] = (byte)(value ? 0 : 1);
            }
        }

        protected internal EncryptedWire(EncryptedCircuit circuit, int id)
            : base(circuit, id)
        {
            Zero = new EncryptedBit(circuit.Config);
            One = new EncryptedBit(circuit.Config);
            Random.GetBytes(Zero);
            Random.GetBytes(One);
            Permutation = (Zero[-1] & 0x1) == 1;
        }

        protected internal EncryptedWire(EncryptedCircuit circuit, Connection conn)
            : base(circuit, conn)
        {
            Zero = new EncryptedBit(circuit.Config);
            One = new EncryptedBit(circuit.Config);
        }

        public EncryptedBit this[bool bit] => bit ? One : Zero;
        public bool this[EncryptedBit bit]
        {
            get
            {
                if (bit == Zero)
                    return false;
                if (bit == One)
                    return true;
                throw new ArgumentException();
            }
        }

        public void ReadSecrets(Connection conn)
        {
            conn.Read(Zero);
            conn.Read(One);
        }

        public void WriteSecrets(Connection conn)
        {
            conn.Write(Zero);
            conn.Write(One);
        }

        int IObliviousDataSource.Length => 2;
        byte[] IObliviousDataSource.this[int index] => index == 1 ? One : Zero;
    }
}
