﻿using SecureComputation.Components.Circuits;
using SecureComputation.Utils;

namespace SecureComputation.Components.Wires
{
    public abstract class WireBase<C> : Component<C> where C : ICircuit
    {
        protected internal WireBase(C circuit, int id)
            : base(circuit, id)
        { }

        protected internal WireBase(C circuit, Connection conn)
            : base(circuit, conn)
        { }
    }
}
