﻿using System;
using System.Collections.Generic;
using SecureComputation.Components.Circuits;
using SecureComputation.Utils;
// ReSharper disable CoVariantArrayConversion

namespace SecureComputation.Components.Wires
{
    public class Wire : WireBase<Circuit>
    {
        protected internal Wire(Circuit circuit, int id)
            : base(circuit, id)
        { }

        protected internal Wire(Circuit circuit, Connection conn)
            : base(circuit, conn)
        { }

        public static implicit operator Wire(bool value) => new ConstantWire(value);
        public static implicit operator bool(Wire wire)
        {
            switch(wire)
            {
                case ConstantWire cWire:
                    return cWire.Value;
                default:
                    throw new NotSupportedException();
            }
        }

        protected static Wire Invert(Wire wire)
        {
            switch(wire)
            {
                case ConstantWire cWire:
                    return !cWire.Value;
                case InvertedWire iWire:
                    return iWire.Source;
                default:
                    return new InvertedWire(wire);
            }
        }
        protected static Wire Unary(Func<bool, bool> func, Wire w)
        {
            int index = (func(true) ? 2 : 0) | (func(false) ? 1 : 0);
            switch (index)
            {
                case 0: return false;
                case 1: return Invert(w);
                case 2: return w;
                case 3: return true;
                default: throw new NotImplementedException();
            }
        }
        protected static Wire Operator(Func<bool[], bool> func, params Wire[] wires)
        {
            //any variables with the prefix reduced refer to
            //  objects after constants are removed

            //for each reduced index, stores the full index
            var map = new Dictionary<int, int>();

            //the list of reduced wires
            var reducedWires = new List<Wire>(wires);

            //stores the full bit inputs
            var inputs = new bool[wires.Length];

            for (int i = 0, j = 0; i < reducedWires.Count; i++, j++)
            {
                //if a wire is constant, set it in the inputs and remove it
                if (reducedWires[i] is ConstantWire)
                {
                    inputs[j] = reducedWires[i];
                    reducedWires.RemoveAt(i--);
                }
                else
                {
                    //add a mapping of the inputs to outputs
                    map[i] = j;
                }
            }

            //if entirely constants, return immediately
            if (reducedWires.Count == 0)
                return func(inputs);
            //if only one result, use the unary function which avoids creating a new gate
            if(reducedWires.Count == 1)
            {
                return Unary(reducedInput =>
                {
                    foreach (var mapping in map)
                        inputs[mapping.Value] = reducedInput;
                    return func(inputs);
                }, reducedWires[0]);
            }

            ICircuit circuit = reducedWires[0].Circuit;
            for (int i = 1; i < reducedWires.Count; i++)
            {
                if (!ReferenceEquals(circuit, reducedWires[i].Circuit))
                    throw new ArgumentException();
            }

            //stores the full indices to invert
            var inversions = new List<int>();
            for (int i = 0; i < reducedWires.Count; i++)
            {
                if (reducedWires[i] is InvertedWire iWire)
                {
                    reducedWires[i] = iWire.Source;
                    inversions.Add(map[i]);
                }
            }

            //if there are no constants or inversions, just use func directly
            if (reducedWires.Count == wires.Length && inversions.Count == 0)
                return circuit.GetGate(func, wires) as Wire;

            return circuit.GetGate(reducedInputs =>
            {
                //map the reduced inputs to the original array
                foreach (var mapping in map)
                    inputs[mapping.Value] = reducedInputs[mapping.Key];
                //invert all inversion indices
                foreach (int inversion in inversions)
                    inputs[inversion] = !inputs[inversion];
                return func(inputs);
            }, reducedWires.ToArray()) as Wire;
        }

        public static Wire operator !(Wire w) => Unary(a => !a, w);
        public static Wire operator &(Wire w1, Wire w2) => Operator(arr => arr[0] & arr[1], w1, w2);
        public static Wire operator |(Wire w1, Wire w2) => Operator(arr => arr[0] | arr[1], w1, w2);
        public static Wire operator ^(Wire w1, Wire w2) => Operator(arr => arr[0] ^ arr[1], w1, w2);

        public static Wire Xor(params Wire[] wires)
        {
            return Operator(arr =>
            {
                bool value = false;
                foreach (bool bit in arr)
                    value ^= bit;
                return value;
            }, wires);
        }
        public static Wire Majority(params Wire[] wires)
        {
            return Operator(arr =>
            {
                int count = 0;
                foreach (bool bit in arr) if (bit)
                    count++;
                return count > (wires.Length / 2);
            }, wires);
        }

        public static Wire Add(Wire w1, Wire w2, ref Wire carry)
        {
            Wire carryIn = carry;
            carry = Majority(w1, w2, carryIn);
            return Xor(w1, w2, carryIn);
        }
        public static Wire Subtract(Wire w1, Wire w2, ref Wire borrow)
        {
            Wire borrowIn = borrow;
            borrow = Majority(!w1, w2, borrowIn);
            return Xor(w1, w2, borrowIn);
        }
    }

    public class ConstantWire : Wire
    {
        public readonly bool Value;

        protected internal ConstantWire(bool value)
            : base(null, 0)
        {
            Value = value;
        }

        protected override bool Equals(Component<Circuit> other) => Value == ((ConstantWire)other).Value;
        public override int GetHashCode() => Value ? 1 : 0;
    }

    public class InvertedWire : Wire
    {
        public readonly Wire Source;

        protected internal InvertedWire(Wire source)
            : base(source.Circuit, 0)
        {
            Source = source;
        }

        protected override bool Equals(Component<Circuit> other) => Source == ((InvertedWire)other).Source;
        public override int GetHashCode() => ~Source.GetHashCode();
    }
}
