﻿using System;
using System.Security.Cryptography;

namespace SecureComputation
{
    public class Config
    {
        public static readonly Config Default = new Config(8, 64);

        /// <summary>
        /// The size of encrypted values in bytes. Should be at least 8 for optimal security.
        /// </summary>
        public readonly int VarSize;

        /// <summary>
        /// The number of circuits presented to Alice for selection.
        /// The chance of Bob being able to cheat without detection is 1/n.
        /// Should be at least 64 for optimal security.
        /// </summary>
        public readonly int SwitchSize;

        public readonly HashAlgorithm Hash;
        public readonly int HashSize;

        public Config(int varSize, int switchSize)
        {
            if(varSize <= 4)
                throw new ArgumentException();
            if(switchSize <= 32)
                throw new ArgumentException();
            VarSize = varSize;
            SwitchSize = switchSize;
            if (VarSize <= 256 / 8)
            {
                Hash = new SHA256CryptoServiceProvider();
                HashSize = 256 / 8;
            }
            else if (VarSize <= 384 / 8)
            {
                Hash = new SHA384CryptoServiceProvider();
                HashSize = 384 / 8;
            }
            else if(VarSize <= 512 / 8)
            {
                Hash = new SHA512CryptoServiceProvider();
                HashSize = 512 / 8;
            }
            else
            {
                throw new NotSupportedException();
            }
        }
    }
}
