﻿using System;
using System.Security.Cryptography;
using System.Threading;
using SecureComputation.Utils;

namespace SecureComputation.Test
{
    public static class ObliviousTest
    {
        private class OSource : IObliviousDataSource
        {
            public byte[] Zero;
            public byte[] One;

            public int Length => 2;
            public byte[] this[int index] => index == 1 ? One : Zero;
        }


        public static void Execute()
        {
            var rand = new RNGCryptoServiceProvider();
            const int iterations = 100;
            const int dataSize = 16;

            bool success = true;
            Thread receiver = null;
            Thread sender = new Thread(() =>
            {
                try
                {
                    Connection conn = new Connection(null, 11000, true);
                    ObliviousTransfer otrans = new ObliviousTransfer(conn, true, dataSize);
                    OSource src = new OSource
                    {
                        Zero = new byte[dataSize],
                        One = new byte[dataSize]
                    };
                    for(int i = 0; i < iterations; i++)
                    {
                        rand.GetBytes(src.Zero);
                        rand.GetBytes(src.One);
                        conn.WriteBytes(src.Zero);
                        conn.WriteBytes(src.One);
                        otrans.Serve(src);
                    }
                }catch(Exception ex)
                {
                    Console.WriteLine(ex);
                    success = false;
                    // ReSharper disable once PossibleNullReferenceException
                    // ReSharper disable once AccessToModifiedClosure
                    receiver.Abort();
                }
            });
            receiver = new Thread(() =>
            {
                try
                {
                    Connection conn = new Connection(null, 11000, false);
                    ObliviousTransfer otrans = new ObliviousTransfer(conn, false, dataSize);
                    OSource src = new OSource
                    {
                        Zero = new byte[dataSize],
                        One = new byte[dataSize]
                    };
                    var buffer = new byte[1];

                    for (int i = 0; i < iterations; i++)
                    {
                        conn.ReadBytes(src.Zero);
                        conn.ReadBytes(src.One);
                        bool bit = GetRandBool();
                        var received = otrans.Receive(bit ? 1 : 0);
                        var target = bit ? src.One : src.Zero;
                        for (int j = 0; j < dataSize; j++)
                        {
                            if (received[j] != target[j])
                            {
                                throw new CryptographicException(received[0] + " " + target[0]);
                            }
                        }
                        Console.WriteLine("Success " + (i + 1));
                    }

                    bool GetRandBool()
                    {
                        rand.GetBytes(buffer);
                        return (buffer[0] & 1) == 1;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    if(ex is CryptographicException)
                        Console.WriteLine(ex.Message);
                    success = false;
                    sender.Abort();
                }
            });
            sender.Start();
            receiver.Start();
            sender.Join();
            receiver.Join();
            Console.WriteLine(success ? "Success" : "Failed");
            Console.ReadKey();
        }
    }
}
