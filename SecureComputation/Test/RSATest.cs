﻿using System;
using System.Security.Cryptography;
using SecureComputation.Utils;

namespace SecureComputation.Test
{
    // ReSharper disable once InconsistentNaming
    public static class RSATest
    {
        public static void Execute()
        {
            try
            {
                var rand = new RNGCryptoServiceProvider();
                const int iterations = 100;
                const int dataSize = 16;

                var src = new byte[dataSize];
                for (int i = 0; i < iterations; i++)
                {
                    rand.GetBytes(src);
                    Rsa rsa = new Rsa(dataSize + 8);
                    var dest = rsa.Decrypt(rsa.Encrypt(src));
                    for (int j = 0; j < dataSize; j++)
                    {
                        if(src[j] == dest[j])
                            continue;
                        for (int k = 0; k < dataSize; k++)
                            Console.WriteLine(src[k] + " " + dest[k]);
                        throw new Exception();
                    }
                }
                Console.WriteLine("Success");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                Console.WriteLine("Failed");
            }
            Console.ReadKey();
        }
    }
}
