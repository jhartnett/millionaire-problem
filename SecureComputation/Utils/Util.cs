﻿using System;

namespace SecureComputation.Utils
{
    public static class Util
    {
        public static T[] Copy<T>(this T[] arr)
        {
            var arr2 = new T[arr.Length];
            for (int i = 0; i < arr.Length; i++)
                arr2[i] = arr[i];
            return arr2;
        }

        public static O[] CastArr<I, O>(this I[] arr)
        {
            var arr2 = new O[arr.Length];
            for(int i = 0; i < arr.Length; i++)
                arr2[i] = (O)(object)arr[i];
            return arr2;
        }

        public static void Exchg<T>(ref T a, ref T b)
        {
            T t = a;
            a = b;
            b = t;
        }

        private static readonly int[] Log2Lookup;
        static Util()
        {
            Log2Lookup = new int[256];
            for (int i = 1; i < 256; i++)
                Log2Lookup[i] = (int)(Math.Log(i) / Math.Log(2));
        }

        public static int Log2(int i)
        {
            return Log2((uint)i);
        }

        public static int Log2(uint i)
        {
            if (i >= 0x1000000)
                return Log2Lookup[i >> 24] + 24;
            if (i >= 0x10000)
                return Log2Lookup[i >> 16] + 16;
            if (i >= 0x100)
                return Log2Lookup[i >> 8] + 8;
            return Log2Lookup[i];
        }
    }
}
