﻿using System;
using System.Numerics;

namespace SecureComputation.Utils
{
    /// <summary>
    /// A BigInteger-based implementation of RSA. Intentionally does not use the standard library as it automatically
    /// includes a MAC. The security of the oblivious transfer protocol (and therefore secure computation) explicitly
    /// relies on there NOT being a MAC to validate the ciphertext.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class Rsa : ISerializable
    {
        public int Size { get; }
        public int EncryptedSize { get; }
        private BigInteger n;
        private BigInteger e;
        private BigInteger d;

        public byte[] N {
            get => n.ToByteArray();
            set => n = new BigInteger(value);
        }
        public byte[] E {
            get => e.ToByteArray();
            set => e = new BigInteger(value);
        }
        public byte[] D {
            get => d.ToByteArray();
            set => d = new BigInteger(value);
        }

        public Rsa(int size)
        {
            Size = size;
            EncryptedSize = size + 8;
            GenerateKeys(EncryptedSize, out e, out d, out n);
        }

        public Rsa(Connection conn)
        {
            Read(conn);
        }

        public void Read(Connection conn)
        {
            N = conn.ReadByteArr();
            E = conn.ReadByteArr();
        }

        public void Write(Connection conn)
        {
            conn.WriteByteArr(N);
            conn.WriteByteArr(E);
        }

        public void ReadSecrets(Connection conn)
        {
            D = conn.ReadByteArr();
        }

        public void WriteSecrets(Connection conn)
        {
            conn.WriteByteArr(D);
        }

        public byte[] Encrypt(byte[] arr)
        {
            if (arr.Length > Size)
                throw new ArgumentException();
            BigInteger m = ToInt(arr);
            BigInteger c = BigInteger.ModPow(m, e, n);
            return ToArr(c, EncryptedSize);
        }

        public byte[] Decrypt(byte[] arr)
        {
            if (arr.Length != EncryptedSize)
                throw new ArgumentException();
            BigInteger c = ToInt(arr);
            BigInteger m = BigInteger.ModPow(c, d, n);
            return ToArr(m, Size);
        }

        public static BigInteger ToInt(byte[] arr)
        {
            if((arr[arr.Length - 1] & 0x80) != 0)
            {
                var old = arr;
                arr = new byte[old.Length + 1];
                arr.Copy(old);
            }
            return new BigInteger(arr);
        }

        public static byte[] ToArr(BigInteger bint, int len)
        {
            var arr = bint.ToByteArray();
            if(arr.Length != len)
            {
                var old = arr;
                arr = new byte[len];
                arr.Copy(old);
            }
            return arr;
        }

        public static void GenerateKeys(int length, out BigInteger e, out BigInteger d, out BigInteger n)
        {
            BigInteger phi;
            e = 65537;

            do
            {
                BigInteger p = GetPrime(length / 2);
                BigInteger q = GetPrime(length - (length / 2));
                n = p * q;
                phi = n - (p + q - 1);
            } while (BigInteger.GreatestCommonDivisor(e, phi) != 1);

            d = MultiplicativeInverse(e, phi);
        }

        public static BigInteger MultiplicativeInverse(BigInteger a, BigInteger n)
        {
            BigInteger i = n, v = 0, d = 1;
            while(a > 0)
            {
                BigInteger t = i / a;
                BigInteger x = a;
                a = i % x;
                i = x;
                x = d;
                d = v - t * x;
                v = x;
            }
            v %= n;
            if (v < 0)
                v = (v + n) % n;
            return v;
        }

        private static readonly byte[] SmallPrimes = {
            3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47,
            53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107,
            109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167,
            173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229,
            233, 239, 241, 251
        };

        public static BigInteger GetPrime(int length)
        {
            var arr = new byte[length];
            BigInteger n;
            do
            {
                Random.GetBytes(arr);
                arr[arr.Length - 1] &= 0x7f;
                while(arr[arr.Length - 1] == 0)
                {
                    var buffer = new byte[1];
                    Random.GetNonZeroBytes(buffer);
                    arr[arr.Length - 1] = (byte)(buffer[0] & 0x7f);
                }
                //never use even number
                arr[0] |= 1;
                n = new BigInteger(arr);
                bool nonPrime = false;
                foreach (byte prime in SmallPrimes)
                {
                    if(n % prime != 0)
                        continue;
                    nonPrime = true;
                    break;
                }
                if (nonPrime)
                    continue;
                if (RabinMiller(n, 16))
                    break;
            } while (true);
            return n;
        }

        private static BigInteger GetRand(BigInteger min, BigInteger max)
        {
            max -= min;

            var bytes = max.ToByteArray();
            BigInteger num;
            byte topMask = 1 << 7;
            while ((topMask & bytes[bytes.Length - 1]) == 0)
                topMask >>= 1;
            topMask--;

            do
            {
                Random.GetBytes(bytes);
                bytes[bytes.Length - 1] &= topMask; //ensure positive
                num = new BigInteger(bytes);
            } while (num >= max);

            num += min;
            return num;
        }

        private static bool RabinMiller(BigInteger n, int k)
        {
            BigInteger n1 = n - 1;
            BigInteger r = 0;
            BigInteger d = n1;
            while(d.IsEven)
            {
                r++;
                d >>= 1;
            }
            r--;
            while(k-- > 0)
            {
                BigInteger a = GetRand(2, n - 2);
                BigInteger x = BigInteger.ModPow(a, d, n);
                if (x == 1 || x == n1)
                    continue;
                for(BigInteger i = 0; i < r; i++)
                {
                    x = BigInteger.ModPow(x, 2, n);
                    if (x == 1)
                        return false;
                    if (x == n1)
                        goto continu;
                }
                return false;
                continu:;
            }
            return true;
        }
    }
}
