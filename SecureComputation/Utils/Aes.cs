﻿using System.Security.Cryptography;

namespace SecureComputation.Utils
{
    /// <summary>
    /// Performs symmetric encryption using the AES-256 algorithm
    /// </summary>
    public class Aes : ISerializable
    {
        private readonly AesCryptoServiceProvider aes = new AesCryptoServiceProvider();

        private byte[] Key {
            get => aes.Key;
            set => aes.Key = value;
        }
        // ReSharper disable once InconsistentNaming
        private byte[] IV {
            get => aes.IV;
            set => aes.IV = value;
        }

        /// <summary>
        /// Creates a new Aes instance with randomized a randomized secret key.
        /// </summary>
        public Aes()
        {
            aes.GenerateKey();
            aes.GenerateIV();
        }

        /// <summary>
        /// Receives an Aes instance over the given connection.
        /// </summary>
        /// <param name="conn">The connection to receive from.</param>
        public Aes(Connection conn)
        {
            Read(conn);
        }

        public void Read(Connection conn)
        {
            Key = conn.ReadByteArr();
            IV = conn.ReadByteArr();
        }

        public void Write(Connection conn)
        {
            conn.WriteByteArr(Key);
            conn.WriteByteArr(IV);
        }

        /// <summary>
        /// Encrypts the given array.
        /// </summary>
        /// <param name="arr">The plaintext data.</param>
        /// <returns>The ciphertext data.</returns>
        public byte[] Encrypt(byte[] arr)
        {
            using (var encryptor = aes.CreateEncryptor())
                return encryptor.TransformFinalBlock(arr, 0, arr.Length);
        }

        /// <summary>
        /// Decrypts the given array.
        /// </summary>
        /// <param name="arr">The ciphertext data.</param>
        /// <returns>The plaintext data.</returns>
        public byte[] Decrypt(byte[] arr)
        {
            using (var decryptor = aes.CreateDecryptor())
                return decryptor.TransformFinalBlock(arr, 0, arr.Length);
        }
    }
}
