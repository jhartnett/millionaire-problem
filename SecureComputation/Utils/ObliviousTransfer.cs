﻿using System;

namespace SecureComputation.Utils
{
    /// <summary>
    /// Provides a source for oblivious data serving.
    /// </summary>
    public interface IObliviousDataSource
    {
        /// <summary>
        /// The number of items to choose from.
        /// </summary>
        int Length { get; }

        /// <summary>
        /// Getter for the items.
        /// </summary>
        /// <param name="index">The index of the item to get.</param>
        byte[] this[int index] { get; }
    }

    /// <summary>
    /// Implements the oblivious transfer protocol for allowing one actor to select one of a set of blocks of data
    /// without informing the server of the selected item, or seeing the other items.
    /// </summary>
    public class ObliviousTransfer
    {
        /// <summary>
        /// The connection to communicate over.
        /// </summary>
        private readonly Connection conn;

        /// <summary>
        /// The Rsa instance to use.
        /// </summary>
        private readonly Rsa rsa;

        /// <summary>
        /// The size of the plaintext blocks.
        /// </summary>
        public readonly int Size;

        /// <summary>
        /// The size of the encrypted blocks.
        /// </summary>
        public int EncryptedSize => rsa.EncryptedSize;

        /// <summary>
        /// Creates a new oblivious transfer instance over the given connection.
        /// </summary>
        /// <param name="conn">The connection to communicate over.</param>
        /// <param name="server">True to serve data, false to receive.</param>
        /// <param name="size">The size of the plaintext blocks.</param>
        public ObliviousTransfer(Connection conn, bool server, int size)
        {
            this.conn = conn;
            Size = size;
            if (server)
            {
                if (size <= 0)
                    throw new ArgumentException();
                conn.WriteInt(size);
            }
            else
            {
                if (conn.ReadInt() != size)
                    throw new ArgumentException();
            }

            rsa = new Rsa(size);

            if (server)
                conn.Write(rsa);
            else
                conn.Read(rsa);
        }

        /// <summary>
        /// Serves the given oblivious transfer source over the connection.
        /// </summary>
        /// <param name="source">The data source to serve.</param>
        public void Serve(IObliviousDataSource source)
        {
            int len = source.Length;
            conn.WriteInt(len);

            var x = new byte[len][];
            for (int i = 0; i < len; i++)
            {
                x[i] = Random.GetBytes(Size);
                conn.WriteBytes(x[i]);
            }

            var v = conn.ReadBytes(EncryptedSize);

            for (int i = 0; i < len; i++)
            {
                var k = new byte[EncryptedSize];
                Subtract(v, x[i], k);

                k = rsa.Decrypt(k);

                var s = new byte[Size];
                Add(source[i], k, s);
                conn.WriteBytes(s);
            }
        }

        /// <summary>
        /// Receives the ith option in an oblivious transfer.
        /// </summary>
        /// <param name="i">The index of the option to select.</param>
        /// <returns>The data associated with the choice.</returns>
        public byte[] Receive(int i)
        {
            int len = conn.ReadInt();
            int header = i, footer = len - i - 1;

            conn.ReadConsume(header * Size);
            var x = conn.ReadBytes(Size);
            conn.ReadConsume(footer * Size);

            var k = Random.GetBytes(Size);

            var ek = rsa.Encrypt(k);
            var v = new byte[EncryptedSize];
            Add(x, ek, v);
            conn.WriteBytes(v);

            conn.ReadConsume(header * Size);
            var s = conn.ReadBytes(Size);
            conn.ReadConsume(footer * Size);

            var arr = new byte[Size];
            Subtract(s, k, arr);

            return arr;
        }

        private static void Add(byte[] a, byte[] b, byte[] c)
        {
            int carry = 0;
            for (int i = 0; i < c.Length; i++)
            {
                if (i < a.Length)
                    carry += a[i];
                if (i < b.Length)
                    carry += b[i];
                c[i] = (byte)(carry & 0xFF);
                carry >>= 8;
            }
        }

        private static void Subtract(byte[] a, byte[] b, byte[] c)
        {
            int borrow = 0;
            for (int i = 0; i < c.Length; i++)
            {
                if (i < a.Length)
                    borrow += a[i];
                if (i < b.Length)
                    borrow -= b[i];
                c[i] = (byte)(borrow & 0xFF);
                borrow >>= 8;
            }
        }
    }
}
