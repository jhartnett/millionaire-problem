﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace SecureComputation.Utils
{
    /// <summary>
    /// Marks an object which can be sent or received over a connection.
    /// </summary>
    public interface ISerializable
    {
        /// <summary>
        /// Reads the state of this object from the given connection.
        /// </summary>
        /// <param name="conn">The connection to read from.</param>
        void Read(Connection conn);

        /// <summary>
        /// Writes the state of this object to the given connection.
        /// </summary>
        /// <param name="conn">The connection to write to.</param>
        void Write(Connection conn);
    }

    /// <summary>
    /// Provides two-way network communication.
    /// </summary>
    public class Connection : IDisposable
    {
        private readonly Socket socket;
        private readonly NetworkStream stream;
        private readonly BinaryReader reader;
        private readonly BinaryWriter writer;

        /// <summary>
        /// Opens a new connection.
        /// </summary>
        /// <param name="hostname">The host to connect to. Must be a local ip for servers.</param>
        /// <param name="port">The port to use.</param>
        /// <param name="server">True if waiting for a connection, false if connecting to a server.</param>
        public Connection(string hostname, int port, bool server)
        {
            if (hostname == null)
                hostname = Dns.GetHostName();
            IPHostEntry hostInfo = Dns.GetHostEntry(hostname);
            IPAddress address = hostInfo.AddressList[0];
            IPEndPoint endPoint = new IPEndPoint(address, port);

            socket = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            if (server)
            {
                socket.Bind(endPoint);
                socket.Listen(0);

                Socket newSocket = socket.Accept();
                socket.Dispose();
                socket = newSocket;
            }
            else
            {
                socket.Connect(endPoint);
            }
            stream = new NetworkStream(socket);
            reader = new BinaryReader(stream);
            writer = new BinaryWriter(stream);
        }

        public void Dispose()
        {
            socket.Dispose();
            stream.Dispose();
            reader.Dispose();
            writer.Dispose();
        }

        public bool ReadBool()
        {
            return reader.ReadBoolean();
        }

        public byte ReadByte()
        {
            return reader.ReadByte();
        }

        public int ReadInt()
        {
            return reader.ReadInt32();
        }

        public void ReadBools(bool[] arr)
        {
            for(int i = 0; i < arr.Length; i += 8)
            {
                int buffer = reader.ReadByte();
                int len = Math.Min(8, arr.Length - i);
                for (int j = len - 1; j >= 0; j--)
                {
                    arr[i + j] = (buffer & 1) == 1;
                    buffer >>= 1;
                }
            }
        }

        public bool[] ReadBools(int length)
        {
            var arr = new bool[length];
            ReadBools(arr);
            return arr;
        }

        public void ReadBytes(byte[] arr)
        {
            reader.Read(arr, 0, arr.Length);
        }

        public byte[] ReadBytes(int length)
        {
            return reader.ReadBytes(length);
        }

        /// <summary>
        /// Reads and discards data from the connection.
        /// </summary>
        /// <param name="length">The number of bytes to consume.</param>
        public void ReadConsume(int length)
        {
            while (length > 8) { reader.ReadInt64(); length -= 8; }
            while (length > 4) { reader.ReadInt32(); length -= 4; }
            while (length > 2) { reader.ReadInt16(); length -= 2; }
            while (length-- > 0) reader.ReadByte();
        }

        public byte[] ReadByteArr()
        {
            return ReadBytes(ReadInt());
        }

        public bool[] ReadBoolArr()
        {
            return ReadBools(ReadInt());
        }

        public void Read<T>(T readable) where T : ISerializable
        {
            readable.Read(this);
        }

        public void Reads<T>(IEnumerable<T> readables) where T : ISerializable
        {
            foreach (var readable in readables)
                Read(readable);
        }

        public T[] ReadArr<T>(Func<T> factory) where T : ISerializable
        {
            var arr = new T[ ReadInt() ];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = factory();
            return arr;
        }

        public void WriteBool(bool b)
        {
            writer.Write(b);
        }

        public void WriteByte(byte b)
        {
            writer.Write(b);
        }

        public void WriteInt(int i)
        {
            writer.Write(i);
        }

        public void WriteBools(bool[] arr)
        {
            int buffer = 0;
            for (int i = 0; i < arr.Length; i += 8)
            {
                int len = Math.Min(8, arr.Length - i);
                for (int j = 0; j < len; j++)
                    buffer = (buffer << 1) | (arr[i + j] ? 1 : 0);
                writer.Write((byte)buffer);
            }
        }

        public void WriteBytes(byte[] arr)
        {
            writer.Write(arr);
        }

        public void WriteByteArr(byte[] arr)
        {
            WriteInt(arr.Length);
            WriteBytes(arr);
        }

        public void WriteBoolArr(bool[] arr)
        {
            WriteInt(arr.Length);
            WriteBools(arr);
        }

        public void Write<T>(T writable) where T : ISerializable
        {
            writable.Write(this);
        }

        public void Writes<T>(IEnumerable<T> writables) where T : ISerializable
        {
            foreach(var writable in writables)
                Write(writable);
        }

        public void WriteArr<T>(T[] arr) where T : ISerializable
        {
            WriteInt(arr.Length);
            foreach(T value in arr)
                Write(value);
        }
    }
}
