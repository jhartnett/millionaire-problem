﻿using System;

namespace SecureComputation.Utils
{
    public static unsafe class ByteArrayExtensions
    {
        /// <summary>
        /// Copies as much data as possible from the src to the dst.
        /// </summary>
        /// <param name="dst">The destination array.</param>
        /// <param name="src">The source array.</param>
        /// <param name="dstOff">The offset to use in the dst array.</param>
        /// <param name="srcOff">The offset to use in the src array.</param>
        public static void Copy(this byte[] dst, byte[] src, int dstOff = 0, int srcOff = 0)
        {
            int len = Math.Min(dst.Length - dstOff, src.Length - srcOff);
            CopyBlock(dst, src, len, dstOff, srcOff);
        }

        /// <summary>
        /// Copies the given block of data from the src to the dst.
        /// </summary>
        /// <param name="dst">The destination array.</param>
        /// <param name="src">The source array.</param>
        /// <param name="len">The number of bytes to copy.</param>
        /// <param name="dstOff">The offset to use in the dst array.</param>
        /// <param name="srcOff">The offset to use in the src array.</param>
        // ReSharper disable once MethodOverloadWithOptionalParameter
        public static void CopyBlock(this byte[] dst, byte[] src, int len, int dstOff = 0, int srcOff = 0)
        {
            if(len < 0)
                throw new ArgumentException();
            if(srcOff < 0 || srcOff + len > src.Length)
                throw new ArgumentException();
            if(dstOff < 0 || dstOff + len > dst.Length)
                throw new ArgumentException();
            Buffer.BlockCopy(src, srcOff, dst, dstOff, len);
        }

        /// <summary>
        /// Xor's as much data as possible from the input into the output.
        /// </summary>
        /// <param name="output">The output array.</param>
        /// <param name="input">The input array.</param>
        /// <param name="outOff">The offset to use in the output array.</param>
        /// <param name="inOff">The offset to use in the input array.</param>
        public static void XorEquals(this byte[] output, byte[] input, int outOff = 0, int inOff = 0)
        {
            int len = Math.Min(output.Length - outOff, input.Length - inOff);
            XorEqualsBlock(output, input, len, outOff, inOff);
        }

        /// <summary>
        /// Xor's the given block of data from the input into the output.
        /// </summary>
        /// <param name="output">The output array.</param>
        /// <param name="input">The input array.</param>
        /// <param name="len">The number of bytes to xor.</param>
        /// <param name="outOff">The offset to use in the output array.</param>
        /// <param name="inOff">The offset to use in the input array.</param>
        // ReSharper disable once MethodOverloadWithOptionalParameter
        public static void XorEqualsBlock(this byte[] output, byte[] input, int len, int outOff = 0, int inOff = 0)
        {
            if(len < 0)
                throw new ArgumentException();
            if(inOff < 0 || inOff + len > input.Length)
                throw new ArgumentException();
            if(outOff < 0 || outOff + len > output.Length)
                throw new ArgumentException();
            fixed(byte* iPtr = &input[inOff])
            fixed(byte* oPtr = &output[outOff])
            {
                byte* op = oPtr, ip = iPtr;
                while(len-- > 0)
                    *op++ ^= *ip++;
            }
        }

        /// <summary>
        /// Xor's as much data as possible from the inputs into the output.
        /// </summary>
        /// <param name="output">The output array.</param>
        /// <param name="input1">The first input array.</param>
        /// <param name="input2">The second input array.</param>
        /// <param name="outOff">The offset to use in the output array.</param>
        /// <param name="in1Off">The offset to use in the first input array.</param>
        /// <param name="in2Off">The offset to use in the second input array.</param>
        public static void Xor(this byte[] output, byte[] input1, byte[] input2, int outOff = 0, int in1Off = 0, int in2Off = 0)
        {
            int len = Math.Min(output.Length - outOff, Math.Min(input1.Length - in1Off, input2.Length - in2Off));
            XorBlock(output, input1, input2, len, outOff, in1Off, in2Off);
        }

        /// <summary>
        /// Xor's the given block of data from the inputs into the output.
        /// </summary>
        /// <param name="output">The output array.</param>
        /// <param name="input1">The first input array.</param>
        /// <param name="input2">The second input array.</param>
        /// <param name="len">The number of bytes to xor.</param>
        /// <param name="outOff">The offset to use in the output array.</param>
        /// <param name="in1Off">The offset to use in the first input array.</param>
        /// <param name="in2Off">The offset to use in the second input array.</param>
        // ReSharper disable once MethodOverloadWithOptionalParameter
        public static void XorBlock(this byte[] output, byte[] input1, byte[] input2, int len, int outOff = 0, int in1Off = 0, int in2Off = 0)
        {
            if(len < 0)
                throw new ArgumentException();
            if(in1Off < 0 || in1Off + len > input1.Length)
                throw new ArgumentException();
            if(in2Off < 0 || in2Off + len > input2.Length)
                throw new ArgumentException();
            if(outOff < 0 || outOff + len > output.Length)
                throw new ArgumentException();
            fixed(byte* i1Ptr = &input1[in1Off])
            fixed(byte* i2Ptr = &input2[in2Off])
            fixed(byte* oPtr = &output[outOff])
            {
                byte* op = oPtr, ip1 = i1Ptr, ip2 = i2Ptr;
                while(len-- > 0)
                    *op++ = (byte)(*ip1++ ^ *ip2++);
            }
        }

        /// <summary>
        /// Returns true if the given arrays are equal.
        /// </summary>
        /// <param name="arr1">The first array.</param>
        /// <param name="arr2">The second array.</param>
        /// <returns>True if they have the same length and content.</returns>
        public static bool EqualsArr(this byte[] arr1, byte[] arr2)
        {
            int len = arr1.Length;
            if(arr2.Length != len)
                return false;
            for (int i = 0; i < len; i++)
                if (arr1[i] != arr2[i])
                    return false;
            return true;
        }

        /// <summary>
        /// Returns a hashcode for the given array.
        /// </summary>
        /// <param name="arr">The array to hash.</param>
        /// <returns>The hashcode for the content of the array.</returns>
        public static int GetHashCodeArr(this byte[] arr)
        {
            const int prime = 31;
            int len = arr.Length;
            int x = 0;
            for(int i = 0; i < len; i++)
                x = (x * prime) ^ arr[i];
            return x;
        }
    }
}
