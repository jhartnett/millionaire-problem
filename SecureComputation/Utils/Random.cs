using System;
using System.Security.Cryptography;

namespace SecureComputation.Utils
{
    /// <summary>
    /// Cryptographically secure random data source.
    /// </summary>
    public static class Random
    {
        private static readonly RNGCryptoServiceProvider Rand = new RNGCryptoServiceProvider();

        public static byte[] GetBytes(int length)
        {
            var buffer = new byte[length];
            GetBytes(buffer);
            return buffer;
        }
        public static void GetBytes(byte[] buffer) => Rand.GetBytes(buffer);
        public static void GetNonZeroBytes(byte[] buffer) => Rand.GetNonZeroBytes(buffer);

        public static byte GetByte()
        {
            var buffer = GetBytes(1);
            return buffer[0];
        }
        public static short GetShort()
        {
            var buffer = GetBytes(2);
            return (short)((buffer[0] << 8) | buffer[1]);
        }
        public static int GetInt()
        {
            var buffer = GetBytes(4);
            return (buffer[0] << 24) | (buffer[1] << 16) | (buffer[2] << 8) | buffer[3];
        }
        public static long GetLong()
        {
            var buffer = GetBytes(8);
            return (buffer[0] << 56) | (buffer[1] << 48) | (buffer[2] << 40) | (buffer[3] << 32) |
                   (buffer[4] << 24) | (buffer[5] << 16) | (buffer[6] << 8) | buffer[7];
        }
        public static sbyte GetSByte() => (sbyte)GetByte();
        public static ushort GetUShort() => (ushort)GetShort();
        public static uint GetUInt() => (uint)GetInt();
        public static ulong GetULong() => (ulong)GetLong();

        /// <summary>
        /// Returns a random number in [0, max)
        /// </summary>
        /// <param name="max">The maximum number to return, exclusive.</param>
        /// <returns>The random number.</returns>
        public static int GetInt(int max) => GetInt(0, max);

        /// <summary>
        /// Returns a random number in [min, max)
        /// </summary>
        /// <param name="min">The minimum number to return, inclusive.</param>
        /// <param name="max">The maximum number to return, exclusive.</param>
        /// <returns>The random number.</returns>
        public static int GetInt(int min, int max)
        {
            if(min >= max)
                throw new ArgumentException();
            uint size = (uint)(max - min);
            uint ceiling = uint.MaxValue / size * size;
            while(true)
            {
                uint i = GetUInt();
                if(i >= ceiling)
                    continue;
                i %= size;
                return (int)(min + i);
            }
        }
    }
}