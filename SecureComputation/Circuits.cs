﻿using SecureComputation.Components.Circuits;

namespace SecureComputation
{
    public static class Circuits
    {
        /// <summary>
        /// Creates a circuit using the given config which compares two unsigned 32 bit integers,
        /// reporting "greater than" to both actors.
        /// </summary>
        /// <param name="config">The configuration to use.</param>
        /// <returns>The comparison circuit.</returns>
        public static Circuit GetUIntCompareCircuit(Config config)
        {
            Circuit circuit = new Circuit(config);
            //create a 32 bit input for each actor
            var aliceIn = circuit.AliceInput(32);
            var bobIn = circuit.BobInput(32);
            //calculate greater than functions
            var aliceOut = aliceIn > bobIn;
            var bobOut = bobIn > aliceIn;
            //assign outputs
            circuit.AliceOutput(aliceOut);
            circuit.BobOutput(bobOut);
            return circuit;
        }
    }
}
