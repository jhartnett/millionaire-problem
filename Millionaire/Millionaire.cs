﻿using System;
using System.Threading;
using SecureComputation;
using SecureComputation.Actors;
using SecureComputation.Components.Circuits;
using SecureComputation.Utils;

namespace Millionaire
{
    public static class Millionaire
    {
        public static void Main()
        {
            Thread bob = new Thread(() =>
            {
                try
                {
                    Connection conn = new Connection(System.Net.Dns.GetHostName(), 11000, true);
                    uint value = GetUInt("Bob Input: ");
                    conn.WriteBool(true);
                    conn.ReadBool();
                    Actor actor = new Bob(conn);
                    bool hasMore = Compare(actor, value);
                    Console.WriteLine("Bob Output: " + hasMore);

                }catch(Exception ex)
                {
                    Console.WriteLine(ex);
                }
            });

            Thread alice = new Thread(() =>
            {
                try
                {
                    Connection conn = new Connection(System.Net.Dns.GetHostName(), 11000, false);
                    conn.ReadBool();
                    uint value = GetUInt("Alice Input: ");
                    conn.WriteBool(true);
                    Actor actor = new Alice(conn);
                    bool hasMore = Compare(actor, value);
                    Console.WriteLine("Alice Output: " + hasMore);
                }catch(Exception ex)
                {
                    Console.WriteLine(ex);
                }
            });

            bob.Start();
            alice.Start();
        }

        public static bool Compare(Actor actor, uint value)
        {
            Circuit circuit = Circuits.GetUIntCompareCircuit(actor.Config);
            bool[] input = UIntToBools(value);
            bool[] output = actor.Execute(circuit, input);
            bool hasMore = output[0];
            return hasMore;
        }

        public static uint GetUInt(string prompt)
        {
            while(true)
            {
                Console.Write(prompt);
                string input = Console.ReadLine();
                if(uint.TryParse(input, out uint value))
                    return value;
            }
        }

        public static bool[] UIntToBools(uint value)
        {
            var arr = new bool[sizeof(uint) * 8];
            for(int i = 0; i < arr.Length; i++)
                arr[i] = ((value >> i) & 1) == 1;
            return arr;
        }
    }
}
